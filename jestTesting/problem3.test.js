const problem3 = require('../problem3.cjs');
let inventoryObject = require('../inventoryObject.cjs');

describe("problem3", () => {
    test("return an empty array", () => {
        expect(problem3([])).toEqual([])
    });

    test("return an empty array", () => {
        expect(problem3(null)).toEqual([])
    });

    test("return an empty array", () => {
        expect(problem3({})).toEqual([]);
    });

    test('returns sorted car models when inventoryObj is valid', () => {
        const inventoryObj = [
            { car_model: 'BMW' },
            { car_model: 'Audi' },
            { car_model: 'Mercedes' }
        ];
        const result = ['Audi', 'BMW', 'Mercedes'];
        expect(problem3(inventoryObj)).toEqual(result);
    });
})



