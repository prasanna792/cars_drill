let inventoryObject= require('./inventoryObject.cjs');

function problem1(inventoryObj=inventoryObject,val)
{
    
    if(inventoryObj.length==0)
    {
        return inventoryObj=[];
    }
    if(val==null || typeof(val)!='number')
    {
        return [];
    } 
    if (!Array.isArray(inventoryObj)) {
        return inventoryObj=[];
    }
    
    
    for(let index=0;index<inventoryObj.length;index++)
     {
        if(inventoryObj[index].id == val)
        {
            return inventoryObj[index];
            // return `Car 33 is a ${inventoryObj[index].car_year} ${inventoryObj[index].car_make} ${inventoryObj[index].car_model}`
        }
     }
}



module.exports= problem1;