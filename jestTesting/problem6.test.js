const problem6 = require('../problem6.cjs');
let inventoryObject = require('../inventoryObject.cjs');

describe('problem6', () => {
  test('should return an empty array when inventoryObj is empty', () => {
    const inventoryObj = [];
    const result = problem6(inventoryObj);
    expect(result).toEqual([]);
  });

  test('should return an empty array when inventoryObj is not an array', () => {
    const inventoryObj = {};
    const result = problem6(inventoryObj);
    expect(result).toEqual([]);
  });

  test('should return an array of BMW and Audi cars', () => {
    const inventoryObj = [
      { car_make: 'BMW', car_model: 'X5' },
      { car_make: 'Audi', car_model: 'A4' },
      { car_make: 'Mercedes', car_model: 'C-Class' },
      { car_make: 'BMW', car_model: '3 Series' },
      { car_make: 'Tesla', car_model: 'Model S' },
    ];
    const result = problem6(inventoryObj);
    const expectedAnswer = JSON.stringify([
      { car_make: 'BMW', car_model: 'X5' },
      { car_make: 'Audi', car_model: 'A4' },
      { car_make: 'BMW', car_model: '3 Series' },
    ]);
    expect(result).toEqual(expectedAnswer);
  });

  test('should return an empty array when there are no BMW or Audi cars', () => {
    const inventoryObj = [
      { car_make: 'Toyota', car_model: 'Camry' },
      { car_make: 'Honda', car_model: 'Accord' },
      { car_make: 'Ford', car_model: 'Mustang' },
    ];
    const result = problem6(inventoryObj);
    expect(result).toEqual('[]');
  });
});
