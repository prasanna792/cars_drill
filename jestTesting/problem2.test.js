const problem2 = require('../problem2.cjs');
let inventoryObject = require('../inventoryObject.cjs');


describe("problem2", () => {
    test("return an empty array",() => {
        expect(problem2([])).toEqual([]);
    });

    test("return an empty array",() => {
        expect(problem2(null)).toEqual([])
    });

    test("return an empty array",() => {
        expect(problem2({})).toEqual([]);
    });

    test("return the required result",() => {
        let requiredResult='Last car is a Lincoln Town Car';
        expect(problem2(inventoryObject)).toEqual(requiredResult)

    });

})