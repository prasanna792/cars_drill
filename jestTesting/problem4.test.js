const problem4 = require('../problem4.cjs');
let inventoryObject = require('../inventoryObject.cjs');


describe("problem4", () => {
    test("return an empty array", () => {
        expect(problem4([])).toEqual([])
    });

    test("return an empty array", () => {
        expect(problem4(null)).toEqual([])
    });

    test("return an empty array", () => {
        expect(problem4({})).toEqual([]);
    });

    test("return only car year", () => {
        let inventoryObj = [{ "id": 1, "car_make": "Lincoln", "car_model": "Navigator", "car_year": 2009 },
        { "id": 2, "car_make": "Mazda", "car_model": "Miata MX-5", "car_year": 2001 },
        { "id": 3, "car_make": "Land Rover", "car_model": "Defender Ice Edition", "car_year": 2010 },
        { "id": 4, "car_make": "Honda", "car_model": "Accord", "car_year": 1983 },
        { "id": 5, "car_make": "Mitsubishi", "car_model": "Galant", "car_year": 1990 },
        ];

        let expectedResult = [2009, 2001, 2010, 1983, 1990];

        expect(problem4(inventoryObj)).toEqual(expectedResult);
    })

    test("testing if the car year is empty ", () => {
        let inventoryObj = [{ "id": 1, "car_make": "Lincoln", "car_model": "Navigator", "car_year": 2009 },
        { "id": 2, "car_make": "Mazda", "car_model": "Miata MX-5", "car_year": 2001 },
        { "id": 3, "car_make": "Land Rover", "car_model": "Defender Ice Edition", "car_year": "" },
        { "id": 4, "car_make": "Honda", "car_model": "Accord", "car_year": 1983 },
        { "id": 5, "car_make": "Mitsubishi", "car_model": "Galant", "car_year": 1990 },
        ];

        let expectedResult = [2009, 2001, 1983, 1990];

        expect(problem4(inventoryObj)).toEqual(expectedResult);
    })


})