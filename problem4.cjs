let inventoryObject = require('./inventoryObject.cjs');

function problem4(inventoryObj = inventoryObject) {
    if (inventoryObj === null || inventoryObj === undefined) {
        return [];
    }
    if (inventoryObj.length == 0) {
        return inventoryObj = [];
    }
    if (!Array.isArray(inventoryObj)) {
        return inventoryObj = [];
    }
    let allcarYears = [];
    for (let index = 0; index < inventoryObj.length; index++) {
        if (inventoryObj[index].car_year) {
            allcarYears.push(inventoryObj[index].car_year);
        }
    }
    return allcarYears;
}

//let carYears=problem4();
//console.log(carYears);

module.exports = problem4;

