
let inventoryObject = require('../inventoryObject.cjs');
const problem5 = require('../problem5.cjs');

describe('problem5', () => {
  test('returns 0 when allCarYears is an empty array', () => {
    const allCarYears = [];
    const result = problem5(allCarYears);
    expect(result).toBe(0);
  });

  test('returns 0 when allCarYears is not an array', () => {
    const allCarYears = {};
    const result = problem5(allCarYears);
    expect(result).toEqual([]);
  });

  test('returns 0 when all car years are greater than or equal to 2000', () => {
    const allCarYears = [
      { car_year: 2000 },
      { car_year: 2005 },
      { car_year: 2010 },
      { car_year: 2020 }
    ];
    const result = problem5(allCarYears);
    expect(result).toBe(0);
  });

  test('returns the count of cars with years less than 2000', () => {
    const allCarYears = [
      { car_year: 1998 },
      { car_year: 2001 },
      { car_year: 1995 },
      { car_year: 1999 }
    ];
    const result = problem5(allCarYears);
    expect(result).toBe(3);
  });
});
