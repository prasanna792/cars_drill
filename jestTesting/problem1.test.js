

const problem1 = require('../problem1.cjs');
let inventoryObject = require('../inventoryObject.cjs');

describe('problem1', () => {
    // Test case 1: When the inventoryObj is an empty array
    test('returns an empty array', () => {
        expect(problem1([], 33)).toEqual([]);
    });

    // Test case 2: When val is null
    test('returns an empty array', () => {
        expect(problem1(inventoryObject, null)).toEqual([]);
    });

    // Test case 3: When val is not a number
    test('returns an empty array', () => {
        expect(problem1(inventoryObject, 'invalid')).toEqual([]);
    });

    // Test case 4: When inventoryObj is not an array
    test('returns an empty array', () => {
        expect(problem1({}, 33)).toEqual([]);
    });

    // Test case 5: When the desired id is found in inventoryObj
    test('returns the corresponding object from inventoryObj', () => {
        let dataOfId33 = { "id": 33, "car_make": "Jeep", "car_model": "Wrangler", "car_year": 2011 }
        expect(problem1(inventoryObject, 33)).toEqual(dataOfId33);
    });

    // Test case 6: When the desired id is not found in inventoryObj
    test('returns undefined', () => {
        expect(problem1(inventoryObject, 999)).toBeUndefined();
    });
});
