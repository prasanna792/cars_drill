let inventoryObject = require('./inventoryObject.cjs');

function problem3(inventoryObj = inventoryObject) {
   if (inventoryObj === null) {
      return [];
   }

   if (inventoryObj.length == 0) {
      return inventoryObj = [];
   }
   if (!Array.isArray(inventoryObj)) {
      return inventoryObj = [];
   }
   let sortCarModels = [];

   for (let index = 0; index < inventoryObj.length; index++) {
      if (inventoryObj[index].car_model) {
         sortCarModels.push(inventoryObj[index].car_model);
      }
   }

   return sortCarModels.sort();
}
//et carModels=problem3([]);
//console.log(carModels);


module.exports = problem3;