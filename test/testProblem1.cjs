let problem1 = require('../problem1.cjs');
let inventoryObject = require('../inventoryObject.cjs');

let result = problem1(inventoryObject, 33);

if (result) {
    console.log(`Car 33 is a ${result.car_year} ${result.car_make} ${result.car_model}`);
  } else {
    console.log('Car not found');
  }

// console.log(result);

